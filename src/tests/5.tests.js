const { expect, browser, $ } = require('@wdio/globals')

describe('Compute Engine Estimate', () => {

    it('prueba', async () => {
        await browser.url(`https://cloud.google.com/`);
        await $("//div[@class='p1o4Hf']").click();
        await $("//input[@placeholder='Search']").setValue('Google Cloud Platform Pricing Calculator');
        //await $("//i[@aria-label='Search']").waitForClickable();
        await $("//i[@aria-label='Search']").click();
        await $(".K5hUy[target='_self'][href='https://cloud.google.com/products/calculator-legacy?hl=es-419']").click();
        await browser.switchToFrame(await $('//*[@src="https://cloud.google.com/frame/products/calculator-legacy/index_d6a98ba38837346d20babc06ff2153b68c2990fa24322fe52c5f83ec3a78c6a0.frame"]'));
        await browser.switchToFrame(await $("iframe[id='myFrame']"));
    
        await $("//div[@class='tab-holder compute']").click();
        await $("#input_100").setValue('4');
        //What are these instances for?: leave blank
        await $("#input_101").setValue('');
        //Operating System / Software: Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS
        await $("//md-select-value[@id='select_value_label_92']//span[@class='md-select-icon']").click();
        await $("#select_option_102").click();
            //Provisioning model: Regular
        await $("//md-select-value[@id='select_value_label_93']//span[@class='md-select-icon']").click();
        await $("#select_option_115").waitForDisplayed();
        await $("#select_option_115").click();
        
        //Machine Family: General purpose waitForDisplayed
        await $("//md-select-value[@id='select_value_label_94']//span[@class='md-select-icon']").click();
        await $("#select_option_119").waitForDisplayed();
        await $("#select_option_119").click();
        
            //Series: N1 
        await $("//md-select-value[@id='select_value_label_95']//span[@class='md-select-icon']").click();
        await $("#select_option_224").waitForDisplayed()
        await $("#select_option_224").click();
        //Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
        await $("//md-select-value[@id='select_value_label_96']//span[@class='md-select-icon']").click();
        await $("#select_option_474").waitForDisplayed();
        await $("#select_option_474").click();
        //Select “Add GPUs“
        await $("//*[@aria-label='Add GPUs']").click();
            //GPU type: NVIDIA Tesla V100
        await $("//md-select[@id='select_510'] ").click();
        await $("#select_option_517").waitForDisplayed();
        await $("#select_option_517").click();
       //Number of GPUs: 1
        await $("//md-select[@id='select_512']").click();
        await $("#select_option_520").waitForDisplayed();
        await $("#select_option_520").click(); 
        //Local SSD: 2x375 Gb
        
        await $("//md-select-value[@id='select_value_label_468']").click(); 
        await $("#select_option_495").waitForDisplayed();
        await $("#select_option_495").click();
            //Datacenter location: Frankfurt (europe-west3)
        await $("//md-select-value[@id='select_value_label_98']").click(); 
        await $("//input[@id='input_132']").waitForDisplayed();
        await $("//input[@id='input_132']").click(); 
        await $("//input[@id='input_132']").setValue('Frankfurt');
        await $("//md-option[@id='select_option_268']").waitForClickable();
        await $("//md-option[@id='select_option_268']").click()


        await $("//form[@name='ComputeEngineForm']//button[@type='button'][normalize-space()='Add to Estimate']").click();
        const total = await $("(//b[contains(text(),'Total Estimated Cost:')])[1]").getText();
        //getting total form estimation
        const total2 = await $("//h2[@class='md-flex ng-binding ng-scope']").getText();
        //total usd estimation number value
        const total3 = await total2.slice(0,-5);
        await expect(total).toEqual('Total Estimated Cost: USD '+ total3 + ' per 1 month');
        await $('//button[@id="Email Estimate"]').click();

        


        await browser.newWindow('https://internxt.com/es/temporary-email');
        
        await $("//button[@class='flex h-full w-full cursor-pointer flex-row items-center justify-between rounded-xl bg-white text-gray-80 shadow-sm  px-4 py-3' and contains(., '@')]").waitForExist()
        let email = await $('//*[@class="flex h-full w-full max-w-[490px] items-center justify-center rounded-xl lg:w-screen border border-gray-20"]').getText();
        
        await browser.switchWindow('cloud.google.com');
        await browser.switchToFrame(await $('//*[@src="https://cloud.google.com/frame/products/calculator-legacy/index_d6a98ba38837346d20babc06ff2153b68c2990fa24322fe52c5f83ec3a78c6a0.frame"]'));
        await browser.switchToFrame(await $("iframe[id='myFrame']"));
        //await $('//input[@id="input_533"]').waitForExist();
        //await $('//input[@id="input_533"]').setValue(email);
        await $('//input[@ng-model="emailQuote.user.email"]').waitForExist();
        await $('//input[@ng-model="emailQuote.user.email"]').setValue(email);
        await $("//button[normalize-space()='Send Email']").click();
        await browser.switchWindow('internxt.com');
    
        await $("//*[@class='flex h-full border-l-2 border-l-primary w-full flex-col px-4 text-start hover:bg-primary hover:bg-opacity-15 null ']").waitForClickable();
        await $("//*[@class='flex h-full border-l-2 border-l-primary w-full flex-col px-4 text-start hover:bg-primary hover:bg-opacity-15 null ']").click();
        const totalemail = await $('//h2[contains(text(),"Estimated Monthly Cost: USD")]').getText();
        const totalemailnum = await totalemail.slice(28);
        await expect(total3).toEqual(totalemailnum);

    })
    
})
