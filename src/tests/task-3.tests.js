//const CloudPage = require('./../po/pages/cloud.page.js'); 
//const cloudPage = new CloudPage()
const { expect, browser, $ } = require('@wdio/globals')

var total3 = '';
describe('Compute Engine Estimate', () => {
    
    it('1. Open https://cloud.google.com/', async () => {
        //goes to cloud google and searches for calculator
        //await cloudPage.open();
        await browser.url(`https://cloud.google.com/`);
    })

    it('2.3 Search for "Google Cloud Platform Pricing Calculator"', async () => {
        await $("//div[@class='p1o4Hf']").click();
        await $("//input[@placeholder='Search']").setValue('Google Cloud Platform Pricing Calculator');
        await $("//i[@aria-label='Search']").waitForClickable();
        await $("//i[@aria-label='Search']").click();
       //*[@data-icon-position="M1Soyc"] 
       // await browser.keys("Enter");
    })

    it('4. Open Google Cloud Platform Pricing Calculator', async () => {
        //Clicks on second search results that takes user to legacy calculator
        // second result is selected because here is were assignment steps can be followed
        const link = await $(".K5hUy[target='_self'][href='https://cloud.google.com/products/calculator-legacy?hl=es-419']");
        await link.click();
    })

    it('5. Click COMPUTE ENGINE at the top of the page.', async () => {
        await browser.switchToFrame(0);
        await browser.switchToFrame(await $("iframe[id='myFrame']"));
        await $("//div[@class='tab-holder compute']").click();
    })

    it('6. Fill out the form with given data:', async () => {
        //set number of instances to 4
        await $("#input_100").setValue('4');
        //What are these instances for?: leave blank
        await $("#input_101").setValue('');
        //Operating System / Software: Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS
        await $("//md-select-value[@id='select_value_label_92']//span[@class='md-select-icon']").click();
        await $("#select_option_102").click();
         //Provisioning model: Regular
        await $("//md-select-value[@id='select_value_label_93']//span[@class='md-select-icon']").click();
        await $("#select_option_115").waitForDisplayed();
        await $("#select_option_115").click();
        
      //Machine Family: General purpose waitForDisplayed
        await $("//md-select-value[@id='select_value_label_94']//span[@class='md-select-icon']").click();
        await $("#select_option_119").waitForDisplayed();
        await $("#select_option_119").click();
        
         //Series: N1 
        await $("//md-select-value[@id='select_value_label_95']//span[@class='md-select-icon']").click();
        await $("#select_option_224").waitForDisplayed()
        await $("#select_option_224").click();
        //Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB)
        await $("//md-select-value[@id='select_value_label_96']//span[@class='md-select-icon']").click();
        await $("#select_option_474").waitForDisplayed();
        await $("#select_option_474").click();
        //Select “Add GPUs“
        await $("//*[@aria-label='Add GPUs']").click();
        //await $("/html[1]/body[1]/md-content[1]/md-card[1]/div[1]/md-card-content[1]/div[2]/div[1]/md-card[1]/md-card-content[1]/div[1]/div[1]/form[1]/div[13]/div[1]/md-input-container[1]/md-checkbox[1]/div[2]").click(); 
        //GPU type: NVIDIA Tesla V100
        await $("//md-select[@id='select_510'] ").click();
        await $("#select_option_517").waitForDisplayed();
        await $("#select_option_517").click();
        //Number of GPUs: 1
        await $("//md-select[@id='select_512']").click();
        await $("#select_option_520").waitForDisplayed();
        await $("#select_option_520").click(); 
        //Local SSD: 2x375 Gb
        
        await $("//md-select-value[@id='select_value_label_468']").click(); 
        await $("#select_option_495").waitForDisplayed();
        await $("#select_option_495").click();
        //Datacenter location: Frankfurt (europe-west3)
        await $("//md-select-value[@id='select_value_label_98']").click(); 
        await $("//input[@id='input_132']").waitForDisplayed();
        await $("//input[@id='input_132']").click(); 
        await $("//input[@id='input_132']").setValue('Frankfurt');
        await $("//md-option[@id='select_option_268']").waitForClickable();
        await $("//md-option[@id='select_option_268']").click()
        /*
        await browser.performActions([{
            type: 'key',
            id: 'keyboard',
            actions: [
            {type: "pause", duration: 1000},
            {type: 'keyDown', value: "\ue015"},
            ]
        }]);
        
        await browser.keys("Enter");*/
        //Committed usage: 1 Year
        await $("//md-select[@id='select_140']").click(); 
        await $("#select_option_138").waitForDisplayed();
        await $("#select_option_138").click(); 
       

    it('7. Click "Add to Estimate".', async () => {
        await $("//form[@name='ComputeEngineForm']//button[@type='button'][normalize-space()='Add to Estimate']").click();
        await browser.pause(100000);
    })
    })

    it('8. There is a line “Total Estimated Cost: USD ${amount} per 1 month” ', async () => {
        //Total Estimated Monthly Cost text
        const total = await $("(//b[contains(text(),'Total Estimated Cost:')])[1]").getText();
        //getting total form estimation
        const total2 = await $("/html[1]/body[1]/md-content[1]/md-card[1]/div[1]/md-card-content[2]/md-card[1]/md-toolbar[1]/div[1]/h2[2]").getText();
        //total usd estimation number value
        total3 = await total2.slice(0,-5);
        await expect(total).toEqual('Total Estimated Cost: USD '+ total3 + ' per 1 month');
    })

    it('10. In a new tab, open email–generating service..', async () => {
        //go to internxt email generator
        await browser.newWindow('https://internxt.com/es/temporary-email');
        await browser.pause(6000);
    })

    it('11.12 Generate and copy a random email', async () => {
        //wait for email generation and copy
        await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/button[1]").click();
    })

    it('13. Return to the calculator and enter the above email into the email field.', async () => {
       // switch back via url match
       await browser.switchWindow('cloud.google.com');
       await browser.switchToFrame(0);
       await browser.switchToFrame(await $("iframe[id='myFrame']"));
       // click on Email estimate in calculator
       await $("//span[normalize-space()='email']").click();
        //go to email field
        await browser.pause(1000);
        await browser.keys("Tab");
        await browser.keys("Tab");
        //await browser.pause(1000);
        //paste generated email
        await browser.keys(['Command', 'V']);
        await browser.pause(1000);
        await browser.keys("Tab");
        await browser.keys("Tab");
        await browser.keys("Tab");
        await browser.keys("Tab");
        await browser.keys("Tab");
        await browser.keys("Tab");
        await browser.keys("Tab");
    })

    it('14. Press "SEND EMAIL".', async () => {
        await browser.keys("Enter");
        //await browser.pause(1000);
    })
    
    it('15. Wait for email and check that the emailed "Total Estimated Monthly Cost" matches the result in the calculator.', async () => {
        //go back to mailbox
        await browser.switchWindow('internxt.com');
        //await browser.pause(3000);
        //Reload inbox
        
        while (await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/button[1]").isExisting() == false) {
            await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/button[1]/*[name()='svg'][1]").click();
            await browser.pause(1000);
          }
        //await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/button[1]").isExisting()
        //await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/button[1]/*[name()='svg'][1]").click();
        await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/button[1]").waitForClickable();
        await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/button[1]").  click();
        //await browser.pause(1000);
        //total email estimate
        const totalemail = await $("/html[1]/body[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[3]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/h2[1]").getText();
        const totalemailnum = await totalemail.slice(28);
        await expect(total3).toEqual(totalemailnum);
    })


})



